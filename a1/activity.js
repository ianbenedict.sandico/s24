db.users.insertMany([
 {
    
     "firstName" : "Jane",
     "lastName" : "Doe",
     "age" : 21.0,
     "contact" : {
         "phone" : "87654321",
         "email" : "janedoe@gmail.com"
     },
     "courses" : [ 
         "CSS", 
         "Javascript", 
         "Python"
     ],
     "department" : "HR"
 },

 {
    
     "firstName" : "Stephen",
     "lastName" : "Hawking",
     "age" : 76.0,
     "contact" : {
         "phone" : "87654321",
         "email" : "stephenhawking@gmail.com"
     },
     "courses" : [ 
         "Python", 
         "React", 
         "PHP"
     ],
     "department" : "HR"
 },

 {
     
     "firstName" : "Neil",
     "lastName" : "Armstrong",
     "age" : 82.0,
     "contact" : {
         "phone" : "87654321",
         "email" : "neilarmstrong@gmail.com"
     },
     "courses" : [ 
         "React", 
         "Laravel", 
         "Sass"
     ],
     "department" : "HR"
 },


 {
    
     "firstName" : "Bill",
     "lastName" : "Gates",
     "age" : 65.0,
     "contact" : {
         "phone" : "12345678",
         "email" : "bill@gmail.com"
     },
     "courses" : [ 
         "PHP", 
         "Laravel", 
         "HTML"
     ],
     "department" : "Operations",
     "status" : "active"
 },


 {
    
     "namearr" : [ 
         {
             "namea" : "juan"
         }, 
         {
             "nameb" : "tamad"
         }
     ]
 }
 ])

db.users.find(
  {
    $or: [
      {
        "firstName": {$regex: "S"}
      },
      {
        "lastName": {$regex: "D"}
      }
    ]
  },
  {
        "firstName": 1,
        "lastName": 1
  }
)

db.users.find(
  {
    $and: [
      {
        "department": "HR"
      },
      {
        "age": {$lte: 70}
      }
    ]
  }
)

db.users.find(
  {
   $and:[
     {
       "firstName": {$regex: "E"}
     },
     {
        "age": {$lte: 30}
     }
    ]
  }  
)